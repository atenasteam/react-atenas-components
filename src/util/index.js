import moment from 'moment';
import 'moment/locale/pt-br';

moment.locale('pt-br');

/**
 * formatAs Recebe um campo e o formata no formato escolhido
 * @param {value} value Valor a ser traduzido
 * @param {type} type Formatação escolhida
 */
export const formatAs = (value, type, casasDecimais = 2) => {
  switch (type) {
    case 'date':
      if (value) {
        return moment(new Date(value).toISOString()).format('DD/MM/YYYY HH:mm')
      } else {
        return ''
      }
    case 'boolean':
      return value ? "Sim" : "Não"
    case 'money':
      return new Intl.NumberFormat('pt-BR', {
        style: 'currency',
        currency: 'BRL',
        minimumFractionDigits: casasDecimais
      }).format(parseFloat(value))
    case 'percent':
      return new Intl.NumberFormat('pt-BR', {
        style: 'percent',
        minimumFractionDigits: casasDecimais
      }).format(parseFloat(value))
    default:
      return value
  }
}