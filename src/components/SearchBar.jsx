import React from 'react'

import styles from './SearchBar.css'

const SearchBar = ({
  searchCallback,
  value,
  placeholder="Digite aqui para pesquisar...",
  small=false
}) => (
  <div className={`${styles.SearchBar} ${small ? styles.small : ''}`}>
    <i className="fas fa-search"></i>
    <input placeholder={placeholder} onChange={searchCallback} value={value}></input>
  </div>
)

export default SearchBar