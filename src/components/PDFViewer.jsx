import React, { Component } from 'react'

import ModalSimple from './ModalSimple.jsx'
import Loading from './Loading.jsx'

import styles from './PDFViewer.module.css'

class PDFViewer extends Component {
  iframeRef = null


  getBrowser = () => {
    let userAgent = navigator.userAgent.toLowerCase();
    if (userAgent.includes('safari')) {
      if (userAgent.includes('chrome')) {
        return 'chrome'
      } else if (userAgent.includes('opera') || userAgent.includes('opera')) {
        return 'opera'
      } else if (userAgent.includes('edge')) {
        return 'edge'
      } else {
        return 'safari'
      }
    } else if (userAgent.includes('firefox')) {
      return 'firefox'
    }

    return 'shit'
  }

  print = () => {
    this.iframeRef.contentWindow.print()
  }

  download = () => {
    const a = document.createElement('a');
    a.style.display = 'none';
    a.href = this.props.pdf;

    // the filename you want
    a.download = `relatorio-${new Date().toISOString()}.pdf`;
    document.body.appendChild(a);
    a.click();
  }

  render() {
    const { pdf, open, onClose, loading } = this.props

    if (loading) {
      return (
        <ModalSimple open={open} onClose={onClose}>
          <div className={styles.wrap}>
            <Loading message='Preparando o Relatório' />
          </div>
        </ModalSimple>
      )
    }

    let style = { display: 'none' }

    if (this.getBrowser() === 'safari')
      style = {}

    return (
      <ModalSimple open={open} onClose={onClose}>
        <div className={styles.wrap}>
          <div className={styles.actions}>
            <button onClick={this.print} title='Imprimir' style={style}>
              <i className='fas fa-print' />
            </button>
            <button onClick={this.download} title='Download' style={style}>
              <i className='fas fa-download' />
            </button>
          </div>
          <iframe
            ref={r => this.iframeRef = r}
            src={pdf}
            type='application/pdf'
            className={styles.embed}
            title='Relatório'
          >
            Não foi possível carregar o componente para exibir o relatório, tente utilizar um browser como Firefox ou Chrome.
          </iframe>
        </div>
      </ModalSimple>
    )
  }
}

export default PDFViewer 