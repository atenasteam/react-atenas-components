import React from 'react'

import styles from './DropDown.css'
import Button from './Button.jsx';

class DropDown extends React.Component {
  state = {
    expand: 0 // 0 não aberto | 1 aberto | 2 fechado
  }

  DropDownRef = new React.createRef()
  ModalRef = new React.createRef()

  toggleDropDown = (e) => {
    if (this.DropDownRef.contains(e.target) && (this.state.expand !== 1)) {
      // clique no botao = cria o modal
      this.setState({ expand: 1 })
      document.body.addEventListener('click', this.toggleDropDown, true)
    } else if (this.DropDownRef.contains(e.target) && (this.state.expand === 1)) {
      // modal existe e clique no botão = o modal deixa de existir
      this.setState({ expand: 2 })
      document.body.removeEventListener('click', this.toggleDropDown, true)

      // evita que o evento de clique no botão aconteça duas vezes
      e.stopPropagation()
    } else if (this.ModalRef && this.ModalRef.contains(e.target)) {
      // clique dentro do modal = não faz nada
    } else {
      this.setState({ expand: 2 })
      document.body.removeEventListener('click', this.toggleDropDown, true)
    }
  }

  render() {
    const { title, children } = this.props

    let className = ''

    switch (this.state.expand) {
      case 2:
        className = styles.closed
        break;
      case 1:
        className = styles.opened
        break;
      default:
        break;
    }

    return (
      <div className={styles.AtenasDropDown}>
        <div className={styles.button} ref={r => this.DropDownRef = r}>
          <Button
            onClick={this.toggleDropDown}
          >
            {title}
          </Button>
        </div>
        <div className={styles.holder + " " + className} ref={r => this.ModalRef = r}>
          {children}
        </div>
      </div>
    )
  }
}

export default DropDown