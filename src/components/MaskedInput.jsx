import React from 'react'
import ReactMaskedInput from 'react-text-mask'

import styles from './MaskedInput.css'

const MaskedInput = props => {
  let inputClass = styles.MaskedInput

  if (props.small)
    inputClass += ' ' + styles.small

  if (props.icon)
    inputClass += ' ' + styles.withIcon

  if (props.error)
    inputClass += ' ' + styles.err

  return (
    <div className={inputClass} style={{ marginTop: props.label ? '20px' : 0 }}>
      {props.icon ? <i className={props.icon} /> : null}
      <ReactMaskedInput {...props} style={{ textAlign: props.align}} />
      <span className={styles.label}>{props.label}</span>
    </div>
  )
}

export default MaskedInput