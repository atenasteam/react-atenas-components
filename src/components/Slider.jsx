import React from 'react'

import styles from './Slider.css'

const Slider = ({ onChange, onMouseUp, min, max, value }) => (
  <input
    type="range"
    min={min}
    max={max}
    value={value}
    className={styles.Slider}
    onChange={(e) => onChange(e.target.value)}
    onMouseUp={(e) => onMouseUp(e.target.value)}
    onMouseOut={(e) => onMouseUp(e.target.value)} />
)

export default Slider