import React from 'react'

import styles from './Tab.css'

const Tab = ({ children, active, onClick }) => (
  <div className={`${styles.Tab} ${active ? styles.active : ''}`} onClick={onClick}>{children}</div>
)

export default Tab