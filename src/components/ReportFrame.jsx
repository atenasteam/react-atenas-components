import React from "react";
import ReactDOM from "react-dom";
import Button from "./Button.jsx";

import styles from './ReportFrame.css'

class ReportFrame extends React.Component {
  state = {
    renderContent: false
  }

  constructor(props) {
    super(props)

    this.onPreparePrint = this.onPreparePrint.bind(this)
  }

  componentDidMount() {
    this.iframeRoot = this.node.contentDocument.body;
    this.forceUpdate();
  }

  async onPreparePrint() {
    await this.setState({renderContent: true})
    this.node.contentWindow.print()
    await this.setState({renderContent: false})
  }

  render() {
    const { children } = this.props;
    return (
      <div className={styles.ReportFrame}>
        <Button onClick={this.onPreparePrint}>imprimir</Button>
        <iframe
          ref={node => (this.node = node)}
          title="iframe"
          className={styles.hiddenIframe}
        >
          {
            this.state.renderContent
            && this.iframeRoot
            && ReactDOM.createPortal(children, this.iframeRoot)
          }
        </iframe>
      </div>
    );
  }
}

export default ReportFrame
