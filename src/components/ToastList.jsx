import React from 'react'


import styles from './ToastList.css';

import Toast from './Toast.jsx';

const ToastList = ({ toasts, onDestroyToast, style }) => {
  return (
    <div className={styles.ToastList} style={style}>
      {
        toasts
          .map(toast => {
            return <Toast {...toast} key={toast.id} onDestroyToast={onDestroyToast} />
          })
      }
    </div>
  )
}

export default ToastList;