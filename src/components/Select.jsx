import React from 'react'

import styles from './Select.css'

const Select = ({ options, label, onChange, value, required, line, name }) => {
  return (
    <div className={`${styles.AtenasSelect} ${line ? styles.line : ''}`}>
      {label ? <span>{label}:</span> : null}
      <div className={styles.wrap}>
        <select onChange={onChange} value={value || ''} name={name} required={required}>
          <option value="" disabled key={0}>--</option>
          {
            options.map((opt) =>
              <option key={opt.value} value={opt.value}>{opt.label}</option>
            )
          }
        </select>
        <i className="fas fa-caret-down"></i>
      </div>
    </div>
  )
}

export default Select