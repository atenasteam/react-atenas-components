import React from 'react'

import styles from './CheckBox.css'

/**
 * 
 * @param {{value: Boolean, onChange: Function, name: String, titulo: String}} props 
 */
const CheckBox = ({ value, onChange, name, titulo, loading }) => {
  const id = name + '-' + Math.random().toString()

  if (loading) {
    return (
      <div className={styles['AtenasCheckBox']}>
        <input
          className={styles['input'] + ' ' + (loading ? styles['loading'] : '')}
          type="checkbox"
          name={name}
          onChange={() => {}}
          checked={value}
          id={id}
        />
        <label className={styles['label'] + ' ' + (loading ? styles['loading'] : '')} htmlFor={id}>{titulo}</label>
      </div>
    )
  } else {
    return (
      <div className={styles['AtenasCheckBox']}>
        <input
          className={styles['input']}
          type="checkbox"
          name={name}
          onChange={onChange}
          onClick={onChange}
          checked={value}
          id={id}
        />
        <label className={styles['label']} htmlFor={id}>{titulo}</label>
      </div>
    )
  }

}

export default CheckBox