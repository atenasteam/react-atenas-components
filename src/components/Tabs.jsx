import React from 'react'

import styles from './Tabs.css'
import Tab from './Tab.jsx';

class Tabs extends React.Component {
  state = {
    index: 0
  }

  render() {
    return (
      <div className={styles.Tabs}>
        <div className={styles.header}>
          {
            this.props.children.map((child, index) =>
              <Tab
                active={index === this.state.index}
                key={index}
                onClick={() => this.setState({ index })}
              >
                {child.props.label}
              </Tab>
            )
          }
        </div>
        <div className={styles.body}>
          {this.props.children[this.state.index]}
        </div>
      </div>
    )
  }
}

export default Tabs