import React from 'react'

import styles from './Loading.css'

const Loading = ({message, light}) => (
  <div className={`${styles.Loading} ${light ? styles.light : ''}`}>
    <span>{message}</span>
    <div className={styles.skFoldingCube}>
      <div className={`${styles.skCube1} ${styles.skCube}`}></div>
      <div className={`${styles.skCube2} ${styles.skCube}`}></div>
      <div className={`${styles.skCube4} ${styles.skCube}`}></div>
      <div className={`${styles.skCube3} ${styles.skCube}`}></div>
    </div>
  </div>
)

export default Loading