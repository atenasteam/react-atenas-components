// @flow
import React from 'react'
import { AutoSizer, List } from 'react-virtualized';
import { map, forEach, size } from 'lodash'

import styles from './Table.css'
import SearchBar from './SearchBar.jsx';
import Select from './Select.jsx';
import InputDate from './InputDate.jsx';
import Button from './Button.jsx';

import { formatAs } from '../util';

import moment from 'moment';
import 'moment/locale/pt-br';
moment.locale('pt-br');

// Props = {
//   columns: [{
//     // Titulo da coluna
//     label: string,
//     // Atributo do objeto
//     key: string | number,
//     // Largura da coluna '50%', '10px'
//     width: string | number,
//     // função de alteração do input
//     callback: (any, identificador = (string | number)) => mixed,
//     // chave do identificador que é passado á função de callback
//     idKey: (string | number),
//     // tipo do input
//     type: ('number' | 'text' | 'date' | 'datetime-local' | 'email' | 'month' | 'checkbox'),
//     // funcao do render (sobrescreve o input)
//     render: () => mixed,
//     // formatar valor do campo para o formato desejado
//     format: ('date' | 'boolean'),
//     // criar filtro deste tipo para o campo
//     filter: ('select' | 'date'),
//   }],
//   // informa se a tabela pode ser impressa ou nao
//   printable: boolean,
//   // header a ser impresso na impressao
//   renderHeader: JSX,
//   // define se a tabela age internamente ou atraves de um servidor
//   serverside: boolean,
//   // calback para a pesquisa
//   searchCallback: (event) => mixed
//   // valor do campo de pesquisa
//   searchValue: string
//   // Exibe status de carregamento na tabela
//   loading: boolean
//   // callback para carregar a proxima pagina ao atingir o fim do scroll
//   onScrollEnd: () => mixed
// }

// State = {
//   sortBy: string,
//   sortOrder: string,
//   search: string,
//   filtros: object,
//   renderPrint: boolean
// };

class Table extends React.Component {
  state = {
    sortBy: '',
    sortOrder: 'ASC',
    search: '',
    filtros: {},
  }

  filterTimeout = null

  serversideRef = null

  search = async val => {
    await this.setState({ search: val })

    clearTimeout(this.filterTimeout)

    this.filterTimeout = setTimeout(
      this.filterList,
      250
    )
  }

  filterList = () => {
    const { list, columns } = this.props
    const { search, filtros, sortBy, sortOrder } = this.state

    if (!Array.isArray(list)) {
      return []
    }

    let dataset = list

    if (size(filtros)) {
      // se o filtro esta nao nulo, ele é verificado
      forEach(filtros, (filtro, index) => {

        switch (filtro.type) {
          case 'select':
            if (filtro.value) {
              dataset = dataset.filter(item => item[index] === filtro.value)
            }
            break;
          case 'date':
            if (filtro.valueMin && filtro.valueMax) {
              dataset = dataset.filter(item =>
                (item[index] > filtro.valueMin)
                && (item[index] < filtro.valueMax)
              )
            } else if (filtro.valueMin) {
              dataset = dataset.filter(item => item[index] > filtro.valueMin)
            } else if (filtro.valueMax) {
              dataset = dataset.filter(item => item[index] < filtro.valueMax)
            }
            break;
          default:
            break;
        }
      })
    }

    if (search) {
      // pesquisa nas colunas que sao exibidas na tabela
      dataset = dataset.filter(item =>
        columns.map(col => {
          if (typeof (item[col.key]) === 'string')
            return item[col.key].toLowerCase().includes(search.toLowerCase())
          else
            return false
        })
          .reduce((acc, a) => acc || a, false)
      )
    }

    if (sortBy) {
      if (sortOrder === 'ASC') {
        dataset.sort((a, b) => (a[sortBy] > b[sortBy] ? 1 : -1))
      } else {
        dataset.sort((a, b) => (a[sortBy] > b[sortBy] ? -1 : 1))
      }
    }

    return dataset
  }

  init = () => {
    const { list, columns } = this.props

    let filtrosList = {}

    columns.forEach(c => {
      switch (c.filter) {
        case 'select':
          const valores = map(list, item => item[c.key]).filter(a => a)

          filtrosList[c.key] = {
            type: 'select',
            list: [...new Set(valores)].map(item => ({
              value: item,
              label: item
            })),
            value: ''
          }
          break;
        case 'date':
          filtrosList[c.key] = {
            type: 'date',
            valueMin: '',
            valueMax: '',
          }
          break;
        default:
          break;
      }
    })

    this.setState({
      filtros: filtrosList
    })
  }

  UNSAFE_componentWillReceiveProps() {
    this.init()
  }

  componentDidMount() {
    this.init()
  }

  componentDidMount() {
    if (this.serversideRef) {
      this.serversideRef.onscroll = () => {
        // se nao estiver carregando
        if (!this.props.loading) {
          // se a lista não atingir o tamanho máximo
          if (this.props.maxLenght > this.props.list.length) {
            // posição do scroll + altura do cliente
            const posicaoScroll = this.serversideRef.scrollTop + this.serversideRef.clientHeight
            // tamanho da lista * altura da linha
            const posicaoMax = this.props.list.length * 29
            // quantos pixeis antes carrega a proxima pagina
            const threshold = this.serversideRef.clientHeight

            if (posicaoScroll > posicaoMax - threshold) {
              this.props.onScrollEnd()
            }
          }
        }
      }
    }
  }

  onReorder = async sortBy => {
    await this.setState({
      sortBy,
      sortOrder: this.state.sortOrder === 'ASC' ? 'DESC' : 'ASC'
    })
    this.filterList()
  }

  onChangeFiltroSelect = (value, filtro) => {
    this.setState({
      filtros: {
        ...this.state.filtros,
        [filtro]: {
          ...this.state.filtros[filtro],
          value: value
        }
      }
    })

    clearTimeout(this.filterTimeout)

    this.filterTimeout = setTimeout(
      this.filterList,
      250
    )
  }

  onChangeFiltroDateMin = (value, filtro) => {
    this.setState({
      filtros: {
        ...this.state.filtros,
        [filtro]: {
          ...this.state.filtros[filtro],
          valueMin: value,
        }
      }
    })

    clearTimeout(this.filterTimeout)

    this.filterTimeout = setTimeout(
      this.filterList,
      250
    )
  }

  onChangeFiltroDateMax = (value, filtro) => {
    this.setState({
      filtros: {
        ...this.state.filtros,
        [filtro]: {
          ...this.state.filtros[filtro],
          valueMax: value
        }
      }
    })

    clearTimeout(this.filterTimeout)

    this.filterTimeout = setTimeout(
      this.filterList,
      250
    )
  }

  renderFiltros = () => {
    const { columns } = this.props

    return columns.map((c, idx) => {
      switch (c.filter) {
        case 'select':
          return (
            <Select
              options={this.state.filtros[c.key].list}
              key={idx}
              label={c.label}
              onChange={(e) => this.onChangeFiltroSelect(e.target.value, c.key)}
              value={this.state.filtros[c.key].value}
              filtro={c.key} />
          )
        case 'date':
          return (
            <React.Fragment key={idx}>
              <InputDate
                key={1}
                label={`${c.label} Incial`}
                onChange={this.onChangeFiltroDateMin}
                value={this.state.filtros[c.key].valueMin}
                filtro={c.key}
              />
              <InputDate
                key={2}
                label={`${c.label} Final`}
                onChange={this.onChangeFiltroDateMax}
                value={this.state.filtros[c.key].valueMax}
                filtro={c.key}
              />
            </React.Fragment>
          )
        default:
          return null
      }
    })
  }

  renderSimpleHeader = (columns) => {
    return (
      <div className={styles.row + ' ' + styles.header}>
        {
          columns.map((c, idx) => (
            <span
              key={idx}
              title={c.title || c.label}>
              {c.label}
            </span>
          ))
        }
      </div>
    )
  }

  renderHeader = (columns, onReorder, sortBy, sortOrder) => {
    return (
      <tr>

        {
          columns.map((c, idx) => {
            if (c.noSort) {
              return (
                <th
                  key={idx}
                  style={{ width: c.width }}
                  title={c.title || c.label}>
                  {c.label}
                </th>
              )
            } else {
              let icon = null
              if (sortBy === c.key) {
                if (sortOrder === 'ASC')
                  icon = <i className="fas fa-chevron-down"></i>
                else
                  icon = <i className="fas fa-chevron-up"></i>
              }

              return (
                <th
                  className={sortBy === c.key ? styles.ativo : ''}
                  key={idx}
                  style={{ width: c.width }}
                  title={c.title || c.label}
                  onClick={() => onReorder(c.key)}>
                  {icon}{c.label}
                </th>
              )
            }
          })
        }
      </tr>
    )
  }

  renderRow = (item, columns, { key, style, index }) => {
    return (
      <tr className={styles.row} key={key} onClick={() => this.props.onClickItem && this.props.onClickItem(item)}>
        {/* <div className={styles.item} key={key} onClick={() => this.props.onClickItem && this.props.onClickItem(item)}> */}
        {/* <div style={style} className={styles.row}> */}
        {
          columns.map((c, idx) => {
            if (!c.render) {
              if (c.callback) {
                if (c.type === 'checkbox') {
                  return (
                    <td style={{ width: c.width }} key={idx}>
                      <span className={styles.mobileHeader}>{c.label}</span>
                      <input
                        title='Recebido? 🤔'
                        checked={!!item[c.key]}
                        type={c.type}
                        onChange={e => c.callback(e.target.checked, item[c.idKey])}
                      />
                    </td>
                  )
                } else {
                  const value = formatAs(item[c.key], c.format)

                  return (
                    <td style={{ width: c.width }} key={idx}>
                      <span className={styles.mobileHeader}>{c.label}</span>
                      <input
                        title={value}
                        value={value}
                        type={c.type}
                        onChange={e => c.callback(e.target.value, item[c.idKey])}
                      />
                    </td>
                  )
                }
              } else {
                const value = formatAs(item[c.key], c.format)

                return (
                  <td style={{ width: c.width }} key={idx} title={value}>
                    <span className={styles.mobileHeader}>{c.label}</span>
                    <span>{value}</span>
                  </td>
                )
              }

            } else {
              return (
                <td style={{ width: c.width }} key={idx}>
                  <span className={styles.mobileHeader}>{c.label}</span>
                  <span>{c.render({ key: item[c.key], index: index })}</span>
                </td>
              )
            }
          })
        }
      </tr>
    )
  }

  renderList = (list, columns, loading, serverside, contain) => {
    if (!serverside && contain) {
      return (
        <AutoSizer>
          {({ height, width }) => (
            <List
              width={width}
              height={height}
              sortBy={this.state.sortBy}
              sortOrder={this.state.sortOrder}
              rowHeight={30}
              rowCount={list.length}
              rowRenderer={(props) => (
                this.renderRow(list[props.index], columns, props)
              )}
            />
          )}
        </AutoSizer>
      )
    } else {
      return (
        list.map((i, index) => this.renderRow(i, columns, { key: index, style: null, index: index }))
      )
    }
  }

  renderPrint(onPrint) {
    if (onPrint) {
      return (
        <span style={{ paddingRight: '10px' }}>
          <Button onClick={onPrint} icon="fas fa-print">
            Imprimir
          </Button>
        </span>
      )
    } else {
      return null
    }
  }

  renderRedo(onReload) {
    if (onReload) {
      return (
        <span style={{ paddingRight: '10px' }}>
          <Button onClick={onReload} icon="fas fa-redo" />
        </span>
      )
    } else {
      return null
    }
  }

  render() {
    let {
      columns,
      loading,
      serverside,
      contain,
      searchCallback,
      searchValue,
      list,
      onReorder,
      sortBy,
      sortOrder,
      onPrint,
      onReload
    } = this.props

    if (!serverside) {
      searchCallback = this.search
      searchValue = this.state.search
      list = this.filterList()
      onReorder = this.onReorder
      sortBy = this.state.sortBy
      sortOrder = this.state.sortOrder
    }

    return (
      <div className={styles.Table}>
        <div className={styles.filters}>
          <div className={styles.top}>
            <SearchBar
              searchCallback={e => searchCallback(e.target.value)}
              value={searchValue}
            />
            {this.renderPrint(onPrint)}
            {this.renderRedo(onReload)}
          </div>
          <div className={styles.select}>
            {this.renderFiltros()}
          </div>
        </div>
        <div className={styles.tableContainer}>
          <table>
            <thead>
              {this.renderHeader(columns, onReorder, sortBy, sortOrder)}
            </thead>
            <tbody>
              {this.renderList(list, columns, loading, serverside, contain)}
            </tbody>
          </table>
        </div>
      </div>
    )
  }
}

export default Table
