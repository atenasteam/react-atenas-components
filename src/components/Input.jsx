import React from 'react'
import InputMask from 'react-input-mask';

import styles from './Input.css'

const Input = props => {
  let inputClass = styles.Input

  if (props.small)
    inputClass += ' ' + styles.small

  if (props.icon)
    inputClass += ' ' + styles.withIcon

  if (props.error)
    inputClass += ' ' + styles.err

  if (props.disabled)
    inputClass += ' ' + styles.disabled

  return (
    <div className={inputClass} style={props.label ? { marginTop: '20px' } : {}}>
      {props.icon ? <i className={props.icon} /> : null}
      <InputMask {...props} />
      {props.label ? <span className={styles.label}>{props.label}</span> : null}
    </div>
  )
}

export default Input