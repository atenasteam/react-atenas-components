import React, { Component } from 'react';

import styles from './ModalSimple.css';

class ModalSimple extends Component {
  _ismounted = false
  state = {
    closing: false
  }

  closeOnEscape = e => {
    if (this._ismounted) {
      if (e.key === 'Escape')
        this.onClose()
    }
  }

  componentDidMount() {
    this._ismounted = true
    document.addEventListener('keydown', this.closeOnEscape)
  }

  componentWillUnmount() {
    this._ismounted = false
    document.removeEventListener('keydown', this.closeOnEscape)
    document.scrollingElement.style.overflow = null
  }

  onClose = () => {
    this.setState({ closing: true })

    setTimeout(
      this.destroy,
      500
    )
  }

  destroy = async () => {
    if (this._ismounted) {
      this.props.onClose()
      await this.setState({
        closing: false
      })
    }
  }

  render() {
    const { open, children } = this.props
    const { closing } = this.state

    if (open) {
      document.scrollingElement.style.overflow = 'hidden'

      return (
        <div className={`${styles.ModalSimple} ${closing ? styles.closing : ''}`}>
          <div className={styles.backdrop} onClick={this.onClose} />
          <div className={styles.content}>
            <span>
              <button className={styles.closeButton} onClick={this.onClose}>
                <i className='fas fa-times' />
              </button>
            </span>
            {children}
          </div>
        </div>
      )
    }
    
    document.scrollingElement.style.overflow = null

    return null
  }
}

export default ModalSimple