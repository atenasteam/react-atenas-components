import React from 'react'
import PropTypes from 'prop-types'

import styles from './SelectList.css'
import Loading from './Loading.jsx';

import moment from 'moment';
import 'moment/locale/pt-br';

moment.locale('pt-br');

class SelectList extends React.Component {
  static propTypes = {
    list: PropTypes.array.isRequired,
    selecionado: PropTypes.string,
    onSelectFiltro: PropTypes.func,
    filtro: PropTypes.string,
    loading: PropTypes.bool,
    dark: PropTypes.bool
  }

  state = {
    search: '',
    verMais: (this.props.list.length > 5 ? false : true)
  }

  filterTimeout = null

  filterList = () => {
    const { list } = this.props
    const { search } = this.state

    if (!Array.isArray(list)) {
      return []
    }

    let dataset = list

    if (search) {
      const searchLowerCase = search.toLowerCase();

      // pesquisa nas colunas que sao exibidas na tabela
      dataset = dataset.filter(item =>
        item['value'].toLowerCase().includes(searchLowerCase)
      )
    }

    dataset = dataset.sort((a, b) => (b['quantidade'] - a['quantidade']))

    return dataset
  }

  renderRow = (item, { key, style }) => {
    if (item['value']) {
      return (
        <div
          key={key}
          style={style}
          className={`${styles.row} ${this.props.selecionado === item['value'] ? styles.selecionado : ''}`}
          title={item['value']}
          onClick={() => this.props.onSelectFiltro(this.props.filtro, item['value'])}
        >
          <span className={styles.value}>
            {item['value']}
          </span>
          <span className={styles.quantidade}>
            {item['quantidade']}
          </span>
          <i className="fas fa-times" />
        </div>
      )
    } else {
      return null
    }
  }

  renderList = (list, loading) => {
    if (loading) {
      return <Loading />
    }

    const renderList = this.state.verMais ? list : list.slice(0, 6);

    return (
      <div
        className={styles.body}
        style={{ overflowY: (this.state.verMais ? 'auto' : 'hidden') }}
      >
        {renderList.map((i, index) => this.renderRow(i, { key: index, style: null }))}
      </div>
    )
  }

  UNSAFE_componentWillReceiveProps() {
    this.setState({ verMais: this.props.list.length > 5 ? false : true })
  }

  render() {
    let { loading, dark } = this.props

    const list = this.filterList()

    return (
      <div className={styles.AtenasSelectList + " " + (dark ? styles.dark : '')}>
        <div className={styles.filters}>
          <div className={styles.top}>
            <i className="fas fa-search" />
            <input
              className={styles.searchBar}
              placeholder={`Pesquisar ${this.props.filtro}...`}
              onChange={e => this.setState({ search: e.target.value })}
              value={this.state.search}
            />
          </div>
        </div>
        <div className={styles.tableContainer}>
          {this.renderList(list, loading)}
        </div>
        <div
          className={styles.seeMoar}
          onClick={() => this.setState({ verMais: true })}
          style={{ visibility: this.state.verMais ? 'hidden' : 'visible' }}
        >
          mais itens
        </div>
      </div>
    )
  }
}

export default SelectList