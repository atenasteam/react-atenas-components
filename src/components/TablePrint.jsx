import React from 'react'
import ReportFrame from './ReportFrame.jsx';

import './TablePrint.css'

import { formatAs } from '../util';

const style = {
  table: {
    fontFamily: 'sans-serif',
    border: '1px solid #333',
    borderCollapse: 'collapse',
    width: '100%',
    fontSize: '12px'
  },
  th: {
    border: '1px solid #333',
  },
  td: {
    border: '1px solid #333',
  }
}

const TablePrint = ({columns, data, title, renderHeader}) => {
  const renderedColumns = columns.filter(c => !c.render)
  return (
    <div className="AtenasTablePrint">
      <ReportFrame name="Tabela">
        <style dangerouslySetInnerHTML={{__html: `
          @media print
          {
            a[href]:after { content: none !important; }
            img[src]:after { content: none !important; }
          }

          `}}
        />

        <table className="Table" style={style.table}>
          <thead>
            <tr>
              <th colSpan={renderedColumns.length} style={style.th}>{title}</th>
            </tr>
            {
              renderHeader
              ?
              <tr>
                <th colSpan={renderedColumns.length}>
                  {renderHeader()}
                </th>
              </tr>
              : null
            }
            <tr>
              {
                renderedColumns.map((c, idx) =>
                  <th key={idx} style={style.th}>{c.label}</th>
                )
              }
            </tr>
          </thead>
          <tbody>
            {
              data.map((d, idx) =>
                <tr key={idx}>
                  {
                    renderedColumns.map((c, idx) => {
                      const value = formatAs(d[c.key], c.format)

                      return <td key={idx} style={style.td}>{value}</td>
                    })
                  }
                </tr>
              )
            }
          </tbody>
        </table>
      </ReportFrame>
    </div>
  )
}

export default TablePrint