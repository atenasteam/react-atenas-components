import React from 'react'

import styles from './InputDate.css'

const InputDate = ({filtro, label, onChange, value}) => {
  return (
    <span className={styles.AtenasInputDate}>
      <span>{label}:</span>
      <div className={styles.input}>
        <i className="fas fa-calendar-alt" />
        <input
          type="date"
          onChange={e => onChange(e.target.value, filtro)}
          value={value}
        />
        <i className={`${styles.close} fas fa-times`}
          onClick={e => onChange('', filtro)}
        />
      </div>
    </span>
  )
}

export default InputDate