import React from 'react'

import styles from './Toast.css';

// {id:0, title:'teste', message: "teste", type:"info"},
// {id:1, title:'teste', message: "teste", type:"error"},
// {id:2, title:'teste', message: "teste", type:"warning"},
// {id:3, title:'teste', message: "teste", type:"success"},

class Toast extends React.Component {
  state = {
    closing: false,
    percentage: 100,
    style: 0
  }

  _mounted = false
  interval = null
  timeout = null
  notificacaoRef = null
  bodyRef = null

  resetTimer = () => {
    this.startTimer()
  }

  stopTimer = () => {
    if (this._mounted) {
      this.setState({ percentage: 100 })
    }

    clearInterval(this.interval)
  }

  componentDidMount() {
    this._mounted = true
    this.startTimer()
  }

  componentWillUnmount() {
    this._mounted = false
    clearInterval(this.interval)
  }

  onClose = async () => {
    // evita que o componente tente acessar o estado caso após ele ser fechado
    if (this._mounted) {
      await this.setState({ closing: true })


      setTimeout(
        () => {
          this.props.onDestroyToast(this.props.id)
        }
        , 750)
    }
  }

  startTimer = () => {
    clearInterval(this.interval)

    this.interval = setInterval(() => {
      if (this._mounted) {
        if (this.state.percentage - 1 < 0) {
          this.onClose()
        } else {
          this.setState({
            percentage: this.state.percentage - 1
          })
        }
      }
    }, this.props.timeout / 100);
  }

  render() {
    const { title, message, type, undo, retry } = this.props

    let icon = null
    let color = null
    let style = {}

    if (!this.state.closing) {
      if (this.bodyRef) {
        style['height'] = this.bodyRef.clientHeight + 'px';
      }
    }

    switch (type) {
      case 'success':
        icon = <svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="check" style={{ width: '100%', height: '100%' }} role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path fill="currentColor" d="M173.898 439.404l-166.4-166.4c-9.997-9.997-9.997-26.206 0-36.204l36.203-36.204c9.997-9.998 26.207-9.998 36.204 0L192 312.69 432.095 72.596c9.997-9.997 26.207-9.997 36.204 0l36.203 36.204c9.997 9.997 9.997 26.206 0 36.204l-294.4 294.401c-9.998 9.997-26.207 9.997-36.204-.001z"></path></svg>
        color = '#219653'
        break;
      case 'warning':
        icon = <svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="exclamation" style={{ width: '100%', height: '100%' }} role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 192 512"><path fill="currentColor" d="M176 432c0 44.112-35.888 80-80 80s-80-35.888-80-80 35.888-80 80-80 80 35.888 80 80zM25.26 25.199l13.6 272C39.499 309.972 50.041 320 62.83 320h66.34c12.789 0 23.331-10.028 23.97-22.801l13.6-272C167.425 11.49 156.496 0 142.77 0H49.23C35.504 0 24.575 11.49 25.26 25.199z"></path></svg>
        color = '#ec7a35'
        break;
      case 'error':
        icon = <svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="times" style={{ width: '100%', height: '100%' }} role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 352 512"><path fill="currentColor" d="M242.72 256l100.07-100.07c12.28-12.28 12.28-32.19 0-44.48l-22.24-22.24c-12.28-12.28-32.19-12.28-44.48 0L176 189.28 75.93 89.21c-12.28-12.28-32.19-12.28-44.48 0L9.21 111.45c-12.28 12.28-12.28 32.19 0 44.48L109.28 256 9.21 356.07c-12.28 12.28-12.28 32.19 0 44.48l22.24 22.24c12.28 12.28 32.2 12.28 44.48 0L176 322.72l100.07 100.07c12.28 12.28 32.2 12.28 44.48 0l22.24-22.24c12.28-12.28 12.28-32.19 0-44.48L242.72 256z"></path></svg>
        color = '#BB4343'
        break;
      case 'info':
        icon = <svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="info" style={{ width: '100%', height: '100%' }} role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 192 512"><path fill="currentColor" d="M20 424.229h20V279.771H20c-11.046 0-20-8.954-20-20V212c0-11.046 8.954-20 20-20h112c11.046 0 20 8.954 20 20v212.229h20c11.046 0 20 8.954 20 20V492c0 11.046-8.954 20-20 20H20c-11.046 0-20-8.954-20-20v-47.771c0-11.046 8.954-20 20-20zM96 0C56.235 0 24 32.235 24 72s32.235 72 72 72 72-32.235 72-72S135.764 0 96 0z"></path></svg>
        color = '#2F80ED'
        break;
      default:
        icon = <svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="info" style={{ width: '100%', height: '100%' }} role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 192 512"><path fill="currentColor" d="M20 424.229h20V279.771H20c-11.046 0-20-8.954-20-20V212c0-11.046 8.954-20 20-20h112c11.046 0 20 8.954 20 20v212.229h20c11.046 0 20 8.954 20 20V492c0 11.046-8.954 20-20 20H20c-11.046 0-20-8.954-20-20v-47.771c0-11.046 8.954-20 20-20zM96 0C56.235 0 24 32.235 24 72s32.235 72 72 72 72-32.235 72-72S135.764 0 96 0z"></path></svg>
        color = '#2F80ED'
        break;
    }

    return (
      <div
        className={`${styles.Toast} ${this.state.closing ? styles.closing : ''}`}
        style={{ backgroundColor: color, color: '#fff', ...style }}
        onMouseEnter={this.stopTimer}
        onMouseLeave={this.resetTimer}>
        {/* <div className={styles.icon}>
          {icon}
        </div> */}
        <div className={styles.body} ref={r => this.bodyRef = r}>
          <h3>{title}</h3>
          {
            Array.isArray(message)
              ? message.map((m, i) => <p key={i}>{m}</p>)
              : <p>{message}</p>
          }
          <div className={styles.actions}>
            {
              undo 
              ? (
                <button onClick={() => {undo() && this.onClose()}}>
                  <i className="fas fa-undo-alt"></i>
                  <span> desfazer</span>
                </button>
              )
              : null
            }
            {
              retry
              ? (
                <button onClick={() => {retry() && this.onClose()}}>
                  <i className="fas fa-redo-alt"></i>
                  <span> tentar novamente</span>
                </button>
              )
              : null
            }
          </div>
        </div>
        <div className={styles.close}>
          <button onClick={this.onClose}><i className='fas fa-times' /></button>
        </div>
        <div className={styles.progress} style={{ width: `${this.state.percentage}%` }} />
      </div>
    )
  }
}

export default Toast