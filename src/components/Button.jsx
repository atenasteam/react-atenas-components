import React from 'react'

import styles from './Button.css'

const Button = (props) => {
  let color = { backgroundColor: '#6c757d', color: '#000' }

  if (props.primary)
    color = { backgroundColor: '#007bff', color: '#000' }
  if (props.secondary)
    color = { backgroundColor: '#6c757d', color: '#000' }
  if (props.success)
    color = { backgroundColor: '#28a745', color: '#000' }
  if (props.danger)
    color = { backgroundColor: '#dc3545', color: '#000' }
  if (props.warning)
    color = { backgroundColor: '#ffc107', color: '#000' }
  if (props.info)
    color = { backgroundColor: '#17a2b8', color: '#000' }
  if (props.light)
    color = { backgroundColor: '#f8f9fa', color: '#000' }
  if (props.dark)
    color = { backgroundColor: '#343a40', color: '#000' }
  if (props.link)
    color = { backgroundColor: 'transparent', color: '#000' }

  return (
    <button {...props}
      onClick={props.onClick}
      className={styles.AtenasButton + " " + (props.className ? props.className : '')}
      style={color}
    >
      <i className={props.icon}></i>
      {
        props.children
          ? <span className={props.icon ? styles.withicon : null}>{props.children}</span>
          : null
      }
    </button>
  )
}

export default Button